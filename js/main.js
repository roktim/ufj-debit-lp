/* ==========================================================================
Author:RoktimSazib
url:http://www.codingavatar.com
description:UFJ-LP project
========================================================================== */



$(document).ready(function(){
    $("button").click(function(){
        alert($("div").scrollTop() + " px");
    });
});



var height=$('.header_area').height();
$('a[href*="#"]')
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top-height
        }, 500, function() {
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) {
            return false;
          } else {
            $target.attr('tabindex','-1');
            $target.focus();
          };
        });
      }
    }
  });

/*
* Top banner Image animation
* @author <Utpal Biswas>
* */

$(document).ready(function(){

    $.fn.animateBgPosition = function( x, y, speed ) {
        var pos = this.css( "background-position" ).split( " " );
        this.x = parseInt( pos[0], 10 ) || 0;
        this.y = parseInt( pos[1], 10 ) || 0;
        $.Animation( this, {
            x: x,
            y: y
        }, {
            duration: speed
        }).progress( function( e ) {
            this.css( "background-position", e.tweens[0].now + "px " + e.tweens[1].now + "px" );
        });
        return this;
    };

    var animTime;
    $.fn.personOne = function(){
        animTime = setTimeout(function(){
            $( "#person1" ).animateBgPosition( -68, -481, 0 );
        }, 300);
        animTime =   setTimeout(function(){
            $( "#person1" ).animateBgPosition( -80, -961, 0 );
        }, 600);
        animTime =  setTimeout(function(){
            $( "#person1" ).animateBgPosition( -55, -1441, 0 );
            $('#person1').css("width", "250px");
        }, 900);
        animTime =  setTimeout(function(){
            $( "#person1" ).animateBgPosition( -52, -1921, 0 );
        }, 1200);
        animTime =  setTimeout(function(){
            $( "#person1" ).animateBgPosition( -60, 0, 0 );
            $('#person1').css("width", "235px");
        }, 1500);
    }
    var animTime1;
    $.fn.personTwo = function(){
        animTime1 = setTimeout(function(){
            $( "#person2" ).animateBgPosition( -48, -481, 0 );
        }, 300);
        animTime1 =   setTimeout(function(){
            $( "#person2" ).animateBgPosition( -32, -961, 0 );
            $('#person2').css("width", "220px");
        }, 600);
        animTime1 =  setTimeout(function(){
            $( "#person2" ).animateBgPosition( -35, -1441, 0 );
            $('#person2').css("width", "220px");
        }, 900);
        animTime1 =  setTimeout(function(){
            $( "#person2" ).animateBgPosition( -22, -1921, 0 );
            $('#person2').css("width", "270px");
        }, 1200);
        animTime1 =  setTimeout(function(){
            $( "#person2" ).animateBgPosition( -48, 0, 0 );
            $('#person2').css("width", "208px");
        }, 1500);
    }
    var animTime2;
    $.fn.personThree = function(){
        animTime2 = setTimeout(function(){
            $( "#person3" ).animateBgPosition( -55, -481, 0 );
        }, 300);
        animTime2 =   setTimeout(function(){
            $( "#person3" ).animateBgPosition( -55, -961, 0 );
        }, 600);
        animTime2 =  setTimeout(function(){
            $( "#person3" ).animateBgPosition( -55, -1441, 0 );
        }, 900);
        animTime2 =  setTimeout(function(){
            $( "#person3" ).animateBgPosition( -55, -1921, 0 );
        }, 1200);
        animTime2 =  setTimeout(function(){
            $( "#person3" ).animateBgPosition( -55, 0, 0 );
        }, 1500);
    }
    var animTime3;
    $.fn.personFour = function(){
        animTime3 = setTimeout(function(){
            $( "#person4" ).animateBgPosition( -17, -481, 0 );
        }, 300);
        animTime3 =   setTimeout(function(){
            $( "#person4" ).animateBgPosition( -5, -961, 0 );
        }, 600);
        animTime3 =  setTimeout(function(){
            $( "#person4" ).animateBgPosition( -17, -1441, 0 );
            $('#person4').css("width", "332px");
        }, 900);
        animTime3 =  setTimeout(function(){
            $( "#person4" ).animateBgPosition( -17, -1921, 0 );
        }, 1200);
        animTime3 =  setTimeout(function(){
            $( "#person4" ).animateBgPosition( -17, 0, 0 );
        }, 1500);
    }

    var animTime4;
    $.fn.personFive = function(){
        animTime4 = setTimeout(function(){
            $( "#person5" ).animateBgPosition( -5, -481, 0 );
        }, 300);
        animTime4 =   setTimeout(function(){
            $( "#person5" ).animateBgPosition( -5, -961, 0 );
        }, 600);
        animTime4 =  setTimeout(function(){
            $( "#person5" ).animateBgPosition( -33, -1441, 0 );
        }, 900);
        animTime4 =  setTimeout(function(){
            $( "#person5" ).animateBgPosition( -25, -1921, 0 );
        }, 1200);
        animTime4 =  setTimeout(function(){
            $( "#person5" ).animateBgPosition( -25, 0, 0 );
        }, 1500);
    }

    var animie = 0;
    time = setInterval(function(){
        if(animie == 0){
           $.fn.personOne();
            $.fn.personThree();
            $.fn.personFive();
            animie = 1;
            return;
        }
        if(animie == 1){
            $.fn.personAnimeOne();
            animie = 0;
            return;
        }
        
    },2000);

    $.fn.personAnimeOne = function(){
        $.fn.personTwo();
        $.fn.personFour();
    }
});
/*
* Header animation
* Scroll down to dismiss the header · Scroll up to display header
* @author <Utpal Biswas>
* */
$(document).ready(function() {
    var $win = $(window),
        $header = $('.header_area'),
        headerHeight = $header.outerHeight(),
        startPos = 0;

    $win.on('load scroll', function() {
        var value = $(this).scrollTop();
        if ( value > startPos && value > headerHeight ) {
            $header.css('top', '-' + headerHeight + 'px');
        } else {
            $header.css('top', '0');
        }
        startPos = value;
    });
});